package at.baltzewitsch.exceptions;

public class NumberNotFoundException extends Exception {
	
	public NumberNotFoundException (String reason){
		
		super(reason);
	}

}
