package at.baltzewitsch.summe;

public class Summe {

	  public static String adder(char[] ax, char[] ay) {

	    int carry = 0;

	    if (ax.length > ay.length) {
	      int[] ar = new int[(ax.length + 1)];
	      for (int i = (ax.length - 1); i > ax.length; i--) {
	        if ((ax[i] + ay[i]) > 9) {
	          ar[i] = ((ax[i] + ay[i] + carry) % 10);
	          carry = 1;
	        } else {
	          ar[i] = (ax[i] + ay[i] + carry);
	          carry = 0;
	        }
	      }
	      return ar.toString();

	    } else {
	      int[] ar = new int[(ay.length + 1)];
	      for (int i = (ay.length - 1); i > ay.length; i--) {
	        if ((ax[i] + ay[i]) > 9) {
	          ar[i] = ((ax[i] + ay[i] + carry) % 10);
	          carry = 1;
	        } else {
	          ar[i] = (ax[i] + ay[i] + carry);
	          carry = 0;
	        }

	      }
	      return ar.toString();
	    }

	  }

	}