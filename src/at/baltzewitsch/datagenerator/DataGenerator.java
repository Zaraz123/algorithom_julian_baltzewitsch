package at.baltzewitsch.datagenerator;

import java.util.ArrayList;
import java.util.LinkedList;

public class DataGenerator {

	public static int[] generateRandomData(int size, int minValue, int maxValue) {

		int[] data = new int[size];
		for (int i = 0; i < size; i++) {
			data[i] = (int) (Math.random() * (maxValue - minValue) + minValue);
		}

		return data;
	}

	public static void printData(int[] data) {

		LinkedList<Integer> dataList = new LinkedList<Integer>();

		for (int n : data) {

			dataList.add(n);
		}

		System.out.println(dataList);
	}

	public void DataComparator(LinkedList<Integer> dataList) {

		ArrayList<Integer> sortedList = new ArrayList<Integer>();

		System.out.println(sortedList);
	}
}