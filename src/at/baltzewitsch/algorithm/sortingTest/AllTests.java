package at.baltzewitsch.algorithm.sortingTest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ BubbleSorterTest.class, insertionsorterTest.class })
public class AllTests {

}
