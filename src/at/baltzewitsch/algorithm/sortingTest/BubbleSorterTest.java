package at.baltzewitsch.algorithm.sortingTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import at.baltzewitsch.algorithm.Sorter.BubbleSorter;

public class BubbleSorterTest {
	
	private int[] data = new int[10];
	
	@Before
	public void setUp() throws Exception{
		data[0]=17;
		data[1]=10;
		data[2]=15;
		data[3]=18;
		data[4]=16;
		data[5]=14;
		data[6]=1;
		data[7]=100;
		data[8]=30;
		data[9]=50;
	}
	
	@Test
	public void test() {
		int[] unsorted = data.clone();
		BubbleSorter bs = new BubbleSorter();
		int[] res = bs.sort(unsorted);
		
		assertTrue(isSorted(res));
	}
	
	private boolean isSorted(int[] sorted){
		int old = sorted[0];
		for (int i = 1; i < sorted.length; i++) {
			if (old<sorted[i]){
				old = sorted[i];
			}else{
				return false;
			}
		}
		return true;
	}

}
