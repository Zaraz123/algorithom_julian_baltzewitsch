package at.baltzewitsch.algorithm;

public interface SortAlgorithm {
	
	public int[] sort(int[] data);
	
	public String getName();

}
