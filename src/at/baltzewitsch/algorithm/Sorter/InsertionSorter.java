package at.baltzewitsch.algorithm.Sorter;

import at.baltzewitsch.algorithm.SortAlgorithm;

public class InsertionSorter implements SortAlgorithm {

	@Override
	public int[] sort(int[] data) {
		int temp;
		for (int i = 1; i < data.length; i++) {
			temp = data[i];
			int j = i;
			while (j > 0 && data[j - 1] > temp) {
				data[j] = data[j - 1];
				j--;
			}
			data[j] = temp;
		}
		return data;
	}

	@Override
	public String getName() {
		
		return "InsertionSorter";
	}

}
