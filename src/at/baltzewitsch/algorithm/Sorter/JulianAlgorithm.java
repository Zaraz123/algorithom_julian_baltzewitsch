package at.baltzewitsch.algorithm.Sorter;

import java.util.LinkedList;

import at.baltzewitsch.algorithm.SortAlgorithm;
import at.baltzewitsch.datagenerator.DataGenerator;

public class JulianAlgorithm implements SortAlgorithm {

	public int[] sort(int[] data) {

		LinkedList<Integer> result = new LinkedList<Integer>();
		Boolean first = false;
		int compare = 0;
		for (int i : data) {
			if (first == false) {
				result.add(i);
				compare = i;
				first = true;
			} else if (first == true) {
				if (i == compare) {
					result.add(result.indexOf(compare), i);
				} else if (i > compare) {
					compare = i;
					result.addLast(i);
				} else if (i < compare) {
					int current = result.size();
					for (int p = 0; p < current; p++) {
						if (result.get(p) > i) {
							result.add(result.indexOf(result.get(p)), i);
							break;
						}
					}
				}
			}
		}

		int[] sortedData = new int[result.size()];
		for (int i= 0; i < sortedData.length; i++) {
			sortedData[i] = result.get(i);
		}

	return sortedData;

}

	@Override
	public String getName() {
		
		return "JulianAlgorithm";
	}}