package at.baltzewitsch.algorithm.Sorter;

import at.baltzewitsch.algorithm.SortAlgorithm;

public class BubbleSorter implements SortAlgorithm {
	
	@Override
	public int[] sort(int[] data) {
		
		int temp;
		for(int i=1; i<data.length; i++) {
			for(int j=0; j<data.length-i; j++) {
				if(data[j]>data[j+1]) {
					temp=data[j];
					data[j]=data[j+1];
					data[j+1]=temp;
				}
				
			}
		}
		return data;
	}

	@Override
	public String getName() {
		
		return "bubblesorter";
	}

}
