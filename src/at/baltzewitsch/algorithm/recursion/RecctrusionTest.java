package at.baltzewitsch.algorithm.recursion;

public class RecctrusionTest {
	public static void main(String[] args) {
		System.out.println(add(15,10));
		System.out.println(getFact(3));
		System.out.println(getPotenz(3,3));
		System.out.println(printBinary(2));
		System.out.println(fibunacci(3));
		System.out.println(isPalindrom("anna"));
		printHex(235);
	}

	public static int add(int a, int b) {

		if (a == b) {

			return a;

		} else if (a > b) {

			return add(a - b, b);

		} else {

			return add(a, b - a);
		}
	}
	
	public static int getFact(int num){
		
		if (num == 1){
			return num;
		}else{
			return num * getFact(num-1);
		}
		
	}
	
	public static int getPotenz(int x, int p){
		if (p == 0){
			return 1;
		}else if (p == 1){
			return x;
		}else{
			return x * getPotenz(x, --p);
		}
	}
	
	public static int printBinary(int num){
		int result = num % 2;
		
		if (num == 1 || num == 0){
			return num;
		}else{
			return result + (printBinary(num/2)*10);
		}
	}
	
	public static int fibunacci(int num){
		if(num == 0){
			return 0;
		}else if (num == 1 || num == 2){
			return 1;
		}else{
			return fibunacci(num - 1) + fibunacci(num - 2);
		}
	}
	
	private static void printHex(int num) {
	    if (num <= 16) {
	      System.out.print(Integer.toHexString(num % 16));
	    } else {
	      printHex(num / 16);
	      System.out.print(Integer.toHexString(num % 16));
	    }
	  }
	
	private static boolean isPalindrom(String num){
	    if(num.length() == 0 || num.length() == 1)
	            return true; 
	        if(num.charAt(0) == num.charAt(num.length()-1))
	            return isPalindrom(num.substring(1, num.length()-1));
	        else{
	          return false;
	        }

	  }

}
