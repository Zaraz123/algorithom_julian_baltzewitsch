package at.baltzewitsch.algorithm;

import java.util.ArrayList;
import java.util.List;

import at.baltzewitsch.algorithm.Sorter.JulianAlgorithm;
import at.baltzewitsch.algorithm.Sorter.BubbleSorter;
import at.baltzewitsch.algorithm.Sorter.InsertionSorter;
import at.baltzewitsch.datagenerator.DataGenerator;

public class SpeedTest {

	  private List<SortAlgorithm> algorithms;
	  
	  public SpeedTest() {
	    this.algorithms = new ArrayList<>();
	  }
	  
	  public void addAlgorithm(SortAlgorithm algo) {
	    this.algorithms.add(algo);
	  }
	  
	  public void run() {
	    int[] data = DataGenerator.generateRandomData(40000, 1, 10000);
	    
	    for (SortAlgorithm s : algorithms) {
	      System.out.println(s.getName());
	      
	      final long timeStart = System.currentTimeMillis(); 
	      
	      s.sort(data);
	      
	      final long timeEnd = System.currentTimeMillis(); 
	      System.out.println("Time took to sort: " + (timeEnd - timeStart) + " Millisek.");
	    }
	  }
	  
	  public static void main(String[] args) {
	    SpeedTest st = new SpeedTest();
	    JulianAlgorithm ja = new JulianAlgorithm();
	    BubbleSorter bs = new BubbleSorter();
	    InsertionSorter is = new InsertionSorter();
	   
	    
//	    st.addAlgorithm(ja);
//	    st.addAlgorithm(bs);
	    st.addAlgorithm(is);
	    st.run();
	  }
	}
