package at.baltzewitsch.application;

import java.util.LinkedList;

import at.baltzewitsch.algorithm.SortAlgorithm;
import at.baltzewitsch.algorithm.Searcher.SequenceSearcher;
import at.baltzewitsch.algorithm.Sorter.BubbleSorter;
import at.baltzewitsch.algorithm.Sorter.InsertionSorter;
import at.baltzewitsch.algorithm.Sorter.JulianAlgorithm;
import at.baltzewitsch.datagenerator.DataGenerator;
import at.baltzewitsch.exceptions.NumberNotFoundException;

public class App {

	public static void main(String[] args) {
		int[] test = DataGenerator.generateRandomData(1000, 0, 100);
		
		InsertionSorter sq = new InsertionSorter();
		BubbleSorter bs = new BubbleSorter();
		bs.sort(test);
//		sq.sort(test);
		DataGenerator.printData(test);
	}
}